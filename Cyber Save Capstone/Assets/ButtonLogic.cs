﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//if the button at the bottom of the conversation panel is clicked
//sets UIMgr's boolean to false
//effectively closing the conversation panel
public class ButtonLogic : MonoBehaviour
{
    // Start is called before the first frame update
    public Button yourButton;
    void Start()
    {
    	Button btn = yourButton;
    	btn.onClick.AddListener(TaskOnClick);
    }
    
    void TaskOnClick(){
    	UIMgr.instance.istalking = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
