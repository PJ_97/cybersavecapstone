﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Startup : MonoBehaviour
{
    public bool loadScene = true;
    public SceneReference StartupScene;
    public MenuClassifier LoadingMenuClassfier;

    void Start()
    {
        Input.multiTouchEnabled = false;

        Scene _scene = SceneManager.GetSceneByPath(StartupScene);
        if (_scene.isLoaded == false && loadScene == true)
        {
            StartCoroutine(BootSequence());
        }
        else if (_scene.buildIndex == -1)
        {
            Debug.LogWarning("Scene not found: " + StartupScene);
        }
        else
        {
            StartCoroutine(IgnoreBootSequence());
        }
    }

    IEnumerator IgnoreBootSequence()
    {
        yield return new WaitForSeconds(2);
        SceneLoadedCallback(null);
    }

    IEnumerator BootSequence()
    {
        yield return new WaitForSeconds(2);
        SceneLoader.Instance.onSceneLoadedEvent.AddListener(SceneLoadedCallback);
        SceneLoader.Instance.LoadScene(StartupScene, false);
    }

    void SceneLoadedCallback(List<string> scenesLoaded)
    {
        SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(SceneLoadedCallback);
        MenuManager.Instance.hideMenu(LoadingMenuClassfier);
    }
}
