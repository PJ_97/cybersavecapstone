﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class SceneLoader : Singleton<SceneLoader>
{
    [System.Serializable]
    public class SceneLoadedEvent : UnityEvent<List<string>>
    {
    }
    [HideInInspector] public SceneLoadedEvent onSceneLoadedEvent = new SceneLoadedEvent();
    private List<string> scenesLoaded = new List<string>();

    public MenuClassifier loadingMenuClasifier;

    private float delayTimer = 1.0f;

    /// <summary>
    /// Loads one scene
    /// </summary>
    /// <param name="_loadScene"></param>
    /// <param name="_showLoadingScreen"></param>
    /// <param name="_unloadScene"></param>
    public void LoadScene(string _loadScene, bool _showLoadingScreen = true, string _unloadScene = "")
    {
        StartCoroutine(_LoadScene(_loadScene, _showLoadingScreen, _unloadScene, true));
    }

    /// <summary>
    /// Loads a list of scenes
    /// </summary>
    /// <param name="_loadScenes"></param>
    /// <param name="_showLoadingScreen"></param>
    /// <param name="_unloadScene"></param>
    public void LoadScenes(List<string> _loadScenes, bool _showLoadingScreen = true, string _unloadScene = "")
    {
        StartCoroutine(_LoadScenes(_loadScenes, _showLoadingScreen, _unloadScene));
    }

    /// <summary>
    /// Internal method for loading scenes
    /// </summary>
    /// <param name="_loadScenes"></param>
    /// <param name="_showLoadingScreen"></param>
    /// <param name="_unloadScene"></param>
    /// <returns></returns>
    IEnumerator _LoadScenes(List<string> _loadScenes, bool _showLoadingScreen, string _unloadScene)
    {
        scenesLoaded.Clear();
        foreach (string _loadScene in _loadScenes)
        {
            yield return StartCoroutine(_LoadScene(_loadScene, _showLoadingScreen, _unloadScene, false));
            scenesLoaded.Add(_loadScene);
        }
        onSceneLoadedEvent.Invoke(scenesLoaded);
    }

    IEnumerator _LoadScene(string _scene, bool _showLoadingScreen, string _unloadScene, bool raiseEvent)
    {
        SceneLoader.SceneLoadedEvent _event = new SceneLoadedEvent();

        if (SceneManager.GetSceneByPath(_scene).isLoaded && raiseEvent)
        {
            Debug.Log("Scene is already loaded");

            scenesLoaded.Clear();
            scenesLoaded.Add(_scene);
            onSceneLoadedEvent.Invoke(scenesLoaded);

            yield return null;
        }

        Application.backgroundLoadingPriority = ThreadPriority.Low;

        if (_showLoadingScreen == true)
        {
            MenuManager.Instance.showMenu(loadingMenuClasifier);
        }

        yield return new WaitForSeconds(delayTimer);

        AsyncOperation _sync = null;

        if (_unloadScene != "")
        {
            _sync = SceneManager.UnloadSceneAsync(_unloadScene);
            while (_sync.isDone == false) { yield return null; }
            _sync = Resources.UnloadUnusedAssets();
            while (_sync.isDone == false) { yield return null; }
        }

        yield return new WaitForSeconds(delayTimer);

        _sync = SceneManager.LoadSceneAsync(_scene, LoadSceneMode.Additive);
        while (_sync.isDone == false) { yield return null; }

        yield return new WaitForSeconds(delayTimer);

        Scene _managerScene = SceneManager.GetSceneByPath(_scene);
        if (_managerScene != null && _managerScene.buildIndex != -1)
        {
            UnityEngine.SceneManagement.SceneManager.SetActiveScene(_managerScene);
        }
        else
        {
            Debug.LogWarning("Unable to make the scene Active");
        }

        if (raiseEvent)
        {
            scenesLoaded.Clear();
            scenesLoaded.Add(_scene);
            onSceneLoadedEvent.Invoke(scenesLoaded);
        }

        if (_showLoadingScreen == true)
        {
            MenuManager.Instance.hideMenu(loadingMenuClasifier);
        }

        Application.backgroundLoadingPriority = ThreadPriority.Normal;
    }

    public void UnLoadScene(string _unloadScene)
    {
        StartCoroutine(_UnLoadScene(_unloadScene));
    }

    IEnumerator _UnLoadScene(string _unloadScene)
    {
        AsyncOperation _sync = null;

        _sync = SceneManager.UnloadSceneAsync(_unloadScene);
        while (_sync.isDone == false) { yield return null; }
        _sync = Resources.UnloadUnusedAssets();
        while (_sync.isDone == false) { yield return null; }
    }
}
