﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T instance;

    protected bool initialized;

    protected static bool applicationClosing = false;

    public static T Instance
    {
        get
        {
            if (applicationClosing)
            {
                return null;
            }

            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));

                if (instance == null)
                {
                    GameObject go = new GameObject(typeof(T).Name,typeof(T));
                    instance = go.GetComponent<T>();
                    Debug.Log(go.name + " instance created");
                }
            }

            Debug.Assert(instance !=null, "Instance is null for type: " + typeof(T).ToString());
            return instance;
        }
    }

    public static bool IsValidSingleton()
    {
        return instance != null;
    }

    public static void makeNull()
    {
        Debug.LogWarning("Made Null");
        instance = null;
    }

    public virtual void OnDestroy()
    {

    }
}
