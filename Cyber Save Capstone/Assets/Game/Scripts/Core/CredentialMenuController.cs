﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CredentialMenuController : Menu
{
    [Header("Menu Classifier")]
    public MenuClassifier loadingMenu;
    public MenuClassifier HomeMenu;

    [Header("Canvas Components")]
    public InputField email;
    public InputField password;
    public Toggle rememberBool;
    public Button login;
    public Button register;

    public override void Start()
    {
        base.Start();
        PlayFabManager.Instance.GetEmailAddress(email.text);
        PlayFabManager.Instance.GetPassword(password.text);

        login.onClick.AddListener(delegate { PlayFabManager.Instance.onClickLogIn(email.text, password.text); });        
    }

    public override void onShowMenu(string options)
    {
        base.onShowMenu(options);
        MenuManager.Instance.hideMenu(loadingMenu);
    }

    public override void onHideMenu(string options)
    {
       base.onHideMenu(options);
    }

    private void Update()
    {
        if (PlayFabManager.Instance.logSuccess)
        {
            //MenuManager.Instance.hideMenu(menuClassifier);
            MenuManager.Instance.showMenu(HomeMenu);
        }
    }
}
