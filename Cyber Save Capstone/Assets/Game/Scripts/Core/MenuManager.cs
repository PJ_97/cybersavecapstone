﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MenuManager : Singleton<MenuManager>
{
    public MenuClassifier LoadingScreenMenuClassifier;

    // The string will come from a MenuClassifier
    private Dictionary<string, Menu> MenuList = new Dictionary<string, Menu>();

    public bool StartedFromMenus { get; set; }

    // Use this for initialization
    public void Start()
    {
        if (SceneManager.sceneCount <= 2)
        {
            StartedFromMenus = true;
        }
    }

    public T getMenu<T>(MenuClassifier _menuClassifier) where T : Menu
    {
        if (MenuList.ContainsKey(_menuClassifier.menuName))
        {
            return (T)MenuList[_menuClassifier.menuName];
        }

        return null;
    }

    public void addMenu(Menu _menu, MenuClassifier _menuClassifier)
    {
        if (MenuList.ContainsKey(_menuClassifier.menuName))
        {
            Debug.LogWarning("Menu Exists: " + _menuClassifier.ToString());
        }
        else
        {
            MenuList.Add(_menuClassifier.menuName, _menu);
        }
    }

    public void showMenu(MenuClassifier _menuClassifier, string options = "")
    {
        Menu _menu;
        if (MenuList.TryGetValue(_menuClassifier.menuName, out _menu))
        {
            _menu.onShowMenu(options);
        }
        else
        {
            Debug.Log("Cannot Find Menu Item: " + _menuClassifier.menuName);
        }
    }

    public void hideMenu(MenuClassifier _menuClassifier, string options = "")
    {
        Menu _menu;
        if (MenuList.TryGetValue(_menuClassifier.menuName, out _menu))
        {
            _menu.onHideMenu(options);
        }
        else
        {
            Debug.Log("Cannot find menu: " + _menuClassifier.menuName);
        }
    }

    public void hideAllMenus()
    {
        foreach (var menuItem in MenuList)
        {
            menuItem.Value.onHideMenu("");
        }
    }
}
