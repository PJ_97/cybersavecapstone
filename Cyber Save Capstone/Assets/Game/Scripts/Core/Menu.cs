﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

public class Menu : MonoBehaviour
{
    public MenuClassifier menuClassifier;

    public enum StartMenuState
    {
        Ignore,
        Active,
        Disabled,
    }
    public StartMenuState startMenuState = StartMenuState.Active;

    [Serializable]
    public class RefreshMenuEvent : UnityEvent { }
    public RefreshMenuEvent onRefreshMenu = new RefreshMenuEvent();

    private Animator _animator;

    public bool resetPosition = true;

    public bool IsOpen
    {
        get
        {
            if (_animator != null)
            {
                return _animator.GetBool("isOpen");
            }
            return false;
        }

        set
        {
            if (value == true)
            {
                gameObject.SetActive(true);
                onRefreshMenu.Invoke();
            }
            if (_animator != null)
            {
                _animator.SetBool("isOpen", value);
            }
        }
    }

    public void Refresh()
    {
        onRefreshMenu.Invoke();
    }

    public virtual void onShowMenu(string options)
    {
        IsOpen = true;
    }

    public virtual void onHideMenu(string options)
    {
        IsOpen = false;
    }

    public virtual void AnimationCompleted()
    {
        if (IsOpen == false)
        {
            gameObject.SetActive(IsOpen);
        }
    }

    // Use this for initialization
    public virtual void Awake()
    {
        _animator = GetComponent<Animator>();

        var rect = GetComponent<RectTransform>();
        if (resetPosition == true)
        {
            rect.localPosition = Vector3.zero;
        }
    }

    public virtual void Start()
    {
        MenuManager.Instance.addMenu(this, menuClassifier);

        switch (startMenuState)
        {
            case StartMenuState.Active:
                gameObject.SetActive(true);
                IsOpen = true;
                break;
            case StartMenuState.Disabled:
                gameObject.SetActive(false);
                IsOpen = false;
                break;
        }
    }
}