﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "MenuClassifier", menuName = "Create/UI Menus/Custom Menu Classifier")]
public class MenuClassifier : ScriptableObject
{
    public string menuName;
}

