﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeMenuController : Menu
{
    [Header("Scene References")]
    public SceneReference Demo;

    [Header("Login Canvas Components")]
    public InputField email;
    public InputField password;
    public Toggle rememberBool;
    public Button login;

    [Header("Register Canvas Components")]
    public Button register;
    public InputField registerEmail;
    public InputField registerUserName;
    public InputField registerPassword;

    [Header("Main Canvas Components")]
    public GameObject Home;
    public GameObject Login;
    public GameObject Register;

    
    public override void onShowMenu(string options)
    {
        base.onShowMenu(options);
    }

    public override void onHideMenu(string options)
    {
        base.onHideMenu(options);
    }

    public void onClickLogin()
    {
        if (email != null && password != null)
        {
            PlayFabManager.Instance.onClickLogIn(email.text, password.text);
        }
    }

    public void onClickRegister()
    {
        if (registerEmail != null && registerUserName != null && registerPassword != null)
        {
            PlayFabManager.Instance.onClickRegister(registerEmail.text, registerPassword.text, PlayFabManager.Instance.GetUserName(registerUserName.text));
        }
    }

    private void Update()
    {
        if (PlayFabManager.Instance.registerSuccess)
        {
            PlayFabManager.Instance.registerSuccess = false;
            Register.SetActive(false);
            Login.SetActive(true);
        }

        if (PlayFabManager.Instance.logSuccess)
        {
            PlayFabManager.Instance.logSuccess = false;
            Login.SetActive(false);
            Home.SetActive(true);
        }
    }

    public void OnSceneLoadedCallBack(List<string> scenesLoaded)
    {
        SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(OnSceneLoadedCallBack);
    }

    public void OnTestGameSelected()
    {
        SceneLoader.Instance.onSceneLoadedEvent.AddListener(OnSceneLoadedCallBack);

        List<string> scenes = new List<string>();
        scenes.Add(Demo);
        SceneLoader.Instance.LoadScenes(scenes);
        MenuManager.Instance.hideMenu(menuClassifier);
    }
}

