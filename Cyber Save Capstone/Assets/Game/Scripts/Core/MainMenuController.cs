﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : Menu
{
    [Header("Menu Classifier")]
    public MenuClassifier loadingMenu;

    public override void onShowMenu(string options)
    {
        base.onShowMenu(options);
        MenuManager.Instance.hideMenu(loadingMenu);
    }

    public override void onHideMenu(string options)
    {
        base.onHideMenu(options);
    }
}
