﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//ensures that all speech bubbles face the current direction of the camera
public class SpeechBubbleRotate : MonoBehaviour
{
    // Start is called before the first frame update
    Transform cameraTransform;
    void Start()
    {
    	cameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
    	transform.rotation = Quaternion.LookRotation(transform.position - cameraTransform.position);
    }
}
