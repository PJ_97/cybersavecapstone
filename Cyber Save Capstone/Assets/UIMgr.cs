﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMgr : MonoBehaviour
{
    // Start is called before the first frame update
    //double value used for distance needed for interaction
    const double dist = 1.0;
    //gets a panel and its text for manipulation
    public GameObject textPanel;
    public GameObject textObj;
    //sets default player name
    public string pname = "noname";
    //represents text in panel
    Text text;
    //gets the various objects that need to be referred to or set active
    //or inactive
    public GameObject player, remy, bluesuit, regina, malcolm, stefani, 
    shae, liam, shelf1, monitor1, remyPanel, bluesuitPanel, reginaPanel,
    malcolmPanel, stefaniPanel, shaePanel, liamPanel, shelfPanel1,
    monitorPanel1, remyImage, bluesuitImage, reginaImage, malcolmImage,
    NPCImage, liamImage, shelfImage, monitorImage;
    //boolean used to determine if talking canvas is open
    public bool istalking = false;
    //key used to interact
    public KeyCode kcode;
    //only UIMgr instance
    public static UIMgr instance;
    //makes sure only one UIMgr exists
    public void Awake(){
    	if (instance == null){
    		instance = this;
    	}else{
    		Destroy(this);
    	}
    }
    //sets text to the txt of the conversation panel
    void Start()
    {
    	text = textObj.GetComponent<Text>();
    }
    //clears all images from the conversation panel
    void ResetImages()
    {
    	remyImage.SetActive(false);
    	bluesuitImage.SetActive(false);
    	reginaImage.SetActive(false);
    	malcolmImage.SetActive(false);
    	NPCImage.SetActive(false);
    	liamImage.SetActive(false);
    	shelfImage.SetActive(false);
    	monitorImage.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    	//below the same process is used multiple times to display text:
    	//1. Check if player is close enough to person or object
    	//2. Make panel appear
    	//3. Set the text specific to that person or object
    	//4. Set the notification panel to inactive
    	//5. Reset the conversation panel's image
    	//6. Set active the correct image on the conversaton panel
    	if(Input.GetKeyDown(kcode)){
    		if(Vector3.Distance(player.transform.position, 
    		                    remy.transform.position) <= dist){
    			istalking = true;
    			text.text = "Malware is quite a devastating piece of" +
" software. I doesn’t matter if you have a phone or a laptop you always" +
" need to be careful of what you click on online. That’s why I recommend" +
" getting anti-virus software like Windows defender to get the most" +
" protection from hackers.";
    			remyPanel.SetActive(false);
    			ResetImages();
    			remyImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          bluesuit.transform.position) <= dist){
    			istalking = true;
    			text.text = "You must find the answer to our problem." +
    				" Don't forget to ask your coworkers for help.";
    			bluesuitPanel.SetActive(false);
    			ResetImages();
    			bluesuitImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          regina.transform.position) <= dist){
    			istalking = true;
    			text.text = "Oh hi " + pname + ", I was just taking a look"
    + " at some of these computers to see what we are dealing with here."
    + " This is definitely the work of a certain malware designed to"
    + " damage and destroy our computers and computer systems. The hard"
    + " part is figuring out what it could be, a standard virus, a worm,"
    + " or a trojan horse. I will let you know if I find anything.";
    			reginaPanel.SetActive(false);
    			ResetImages();
    			reginaImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          malcolm.transform.position) <= dist){
    			istalking = true;
    			text.text = "Oh hi rookie, did you need me to help you" +
          " find anything about what kind of Malware we are dealing with" +
          " here? Try looking in bookshelves, computers, or talking to" +
    				"people.";
    			malcolmPanel.SetActive(false);
    			ResetImages();
    			malcolmImage.SetActive(true);
    		}else if((Vector3.Distance(player.transform.position, 
    		                           stefani.transform.position) <= dist)
    		                          || (Vector3.Distance(player.transform.position, 
    		                            shae.transform.position) <= dist)){
    			istalking = true;
    			text.text = "Stefani: Hey did you hear what's been going" +
    				" on about this virus outbreak?\nShae: I know it's crazy." +
    				" You know my computer has been telling me to update my" +
    				" security software and I thought that's no big deal but" +
    				" after seeing this I am definitely going to update my" +
    				" security software.\nStefani: Yeah, and I should check" +
                    " if the firewall on my laptop is on. I hear that's a good" +
    				" way to protect your devices.";
    			stefaniPanel.SetActive(false);
    			shaePanel.SetActive(false);
    			ResetImages();
    			NPCImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          liam.transform.position) <= dist){
    			istalking = true;
    			text.text = "I don’t know how this happened one minute I was"
       + " just going over emails from collegues and all of a sudden my"
       + " computer started acting slower and then just turned off all by"
	   + " itself then all of this this started. What is going on?";
    			liamPanel.SetActive(false);
    			ResetImages();
    			NPCImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          shelf1.transform.position) <= dist){
    			istalking = true;
    			text.text = "Trojan Horse: Oh yeah, that’s a software that" +
" lays dormant on a person's computer waiting for the opportune moment" +
" to strike. Once the software is installed in your system it could spread" +
" through other files and could be used to gain access to your system.";
    			shelfPanel1.SetActive(false);
    			ResetImages();
    			shelfImage.SetActive(true);
    		}else if(Vector3.Distance(player.transform.position, 
    		                          monitor1.transform.position) <= (dist * 2)){
    			istalking = true;
    			text.text = "Spyware: That’s a virus that’s used to observe an" +
" individual’s computer activity. It is a complete invasion of privacy.";
    			monitorPanel1.SetActive(false);
    			ResetImages();
    			monitorImage.SetActive(true);
    		}
    	}
    	//makes sure that if the boolean is true conversation panel is open
    	//and if the boolean is false conversation panel is closed
    	if(istalking){
    		textPanel.SetActive(true);
    	}
    	if(!istalking){
    		textPanel.SetActive(false);
    	}
    }
}
